#include "BatteryManGameMode.h"
#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"


ABatteryManGameMode::ABatteryManGameMode()
{
	PrimaryActorTick.bCanEverTick = true;

	ConstructorHelpers::FClassFinder<APawn> PickableItem(TEXT("/Game/_Main/Blueprints/BP_PickableItem"));
	if (PickableItem.Succeeded())
	{
		PlayerRecharge = PickableItem.Class;
	}

	SpawnXMin = 300.0f;
	SpawnXMax = 2700.0f;
	SpawnYMin = 300.0f;
	SpawnYMax = 3200.0f;
	SpawnZ = 500.0f;
}

void ABatteryManGameMode::SpawnPlayerRecharge()
{
	float SpawnX = FMath::RandRange(SpawnXMin, SpawnXMax);
	float SpawnY = FMath::RandRange(SpawnYMin, SpawnYMax);
	float Rotation = FMath::RandRange(-180.0f, 180.0f);

	FVector SpawnPosition = FVector(SpawnX, SpawnY, SpawnZ);
	FRotator SpawnRotation = FRotator(0.0f, Rotation, 0.0f);

	GetWorld()->SpawnActor(PlayerRecharge, &SpawnPosition, &SpawnRotation);
}


void ABatteryManGameMode::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer(UnusedHandle, this, &ABatteryManGameMode::SpawnPlayerRecharge, FMath::RandRange(2, 4), true);
}

void ABatteryManGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
