#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Blueprint/UserWidget.h"
#include "Camera/CameraComponent.h"
#include "Components/AudioComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "BatteryManCharacter.generated.h"


UCLASS()
class BATTERYMANGAME_API ABatteryManCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABatteryManCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		USpringArmComponent* CameraBoom; //Hold the Camera

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		UCameraComponent* FollowCamera; //Camera Component

	UPROPERTY(EditAnywhere, Category = "HUD")
		TSubclassOf<UUserWidget> PlayerPowerWidgetClass;

	UPROPERTY(EditAnywhere, Category = "HUD")
		TSubclassOf<UUserWidget> EndGameWidgetClass;

	UPROPERTY(EditAnywhere)
		float PowerThreshold;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Power;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 BatteriesCollected;

	UFUNCTION()
		void OnBeginOverlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp,
							int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool bDead;
	UUserWidget* PlayerPowerWidget;
	UUserWidget* EndGameWidget;
	int32 BatteriesCap;

	void MoveForward(float Axis);
	void MoveRight(float Axis);
	void EndGame();
	void IncreaseDifficulty();
};
