#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "BatteryManGameMode.generated.h"


UCLASS()
class BATTERYMANGAME_API ABatteryManGameMode : public AGameMode
{
	GENERATED_BODY()

	ABatteryManGameMode();
		
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float SpawnXMin;

	UPROPERTY(EditAnywhere)
		float SpawnXMax;

	UPROPERTY(EditAnywhere)
		float SpawnYMin;

	UPROPERTY(EditAnywhere)
		float SpawnYMax;

private:
	TSubclassOf<APawn> PlayerRecharge;
	float SpawnZ;

	void SpawnPlayerRecharge();
};
